package com.parkermc.soundevents.events;

import com.parkermc.soundevents.proxy.ProxyCommon;
import com.parkermc.soundevents.ModMain;
import com.parkermc.soundevents.network.MessageConfig;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;

public class EventHandlerServer {
	
	@SubscribeEvent
	public void onConnect(PlayerLoggedInEvent event) {
		ModMain.network.sendTo(new MessageConfig(ProxyCommon.config), (EntityPlayerMP)event.player);
	}
}
