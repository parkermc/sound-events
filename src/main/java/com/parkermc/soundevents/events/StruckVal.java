package com.parkermc.soundevents.events;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class StruckVal {
	public List<UUID> vics = new ArrayList<UUID>();
	public long time;
	
	StruckVal(long time, UUID... vics){
		this.time = time;
		this.vics.addAll(Arrays.asList(vics));
	}
}
