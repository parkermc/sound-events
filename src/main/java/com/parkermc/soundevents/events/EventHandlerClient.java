package com.parkermc.soundevents.events;

import com.parkermc.soundevents.ModConfig;
import com.parkermc.soundevents.ModToolsClient;
import com.parkermc.soundevents.sound.SoundHelper;

import net.minecraftforge.client.event.ClientChatReceivedEvent;
import net.minecraftforge.client.event.ScreenshotEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.FMLNetworkEvent.ClientDisconnectionFromServerEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class EventHandlerClient {	
	/*
	 * Client only ones
	 */
	@SubscribeEvent 
	public void onClientChatReceivedEvent(ClientChatReceivedEvent event){
		if(ModToolsClient.isChatMsg(event)) {
			SoundHelper.playSoundClient(ModToolsClient.getClientPos(), ModConfig.ClientChatReceivedSound);
		}
	}
	
	@SubscribeEvent 
	public void onScreenshotEvent(ScreenshotEvent event){
		SoundHelper.playSoundClient(ModToolsClient.getClientPos(), ModConfig.ScreenshotSound);
	}
	
	@SubscribeEvent 
	public void onClientDisconnectionFromServerEvent(ClientDisconnectionFromServerEvent event){
		SoundHelper.playSoundClient(ModToolsClient.getClientPos(), ModConfig.PlayerLoggedOutSound);
	}
}
