package com.parkermc.soundevents.events;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.parkermc.soundevents.ModConfig;
import com.parkermc.soundevents.ModTools;
import com.parkermc.soundevents.sound.SoundHelper;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.event.entity.EntityMountEvent;
import net.minecraftforge.event.entity.EntityStruckByLightningEvent;
import net.minecraftforge.event.entity.EntityTravelToDimensionEvent;
import net.minecraftforge.event.entity.item.ItemExpireEvent;
import net.minecraftforge.event.entity.item.ItemTossEvent;
import net.minecraftforge.event.entity.living.BabyEntitySpawnEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingJumpEvent;
import net.minecraftforge.event.entity.living.LivingExperienceDropEvent;
import net.minecraftforge.event.entity.player.PlayerPickupXpEvent;
import net.minecraftforge.event.entity.player.PlayerSleepInBedEvent;
import net.minecraftforge.event.entity.player.PlayerWakeUpEvent;
import net.minecraftforge.event.world.BlockEvent.CropGrowEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.ItemCraftedEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.ItemSmeltedEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerChangedDimensionEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedOutEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerRespawnEvent;

public class EventHandler {

	/*
	 * Both client and server side
	 */
	@SubscribeEvent
	public void onItemCraftedEvent(ItemCraftedEvent event) {
		if(ModTools.getWorld(event.player).isRemote) {
			SoundHelper.playSoundClient(new BlockPos(event.player), ModConfig.ItemCraftedSound);
		}else if(ModConfig.ItemCraftedSound.server) {
			SoundHelper.playSoundServerAllBut(new BlockPos(event.player), event.player, event.player.dimension, ModConfig.ItemCraftedSound);
		}	
	}
	
	@SubscribeEvent
	public void onItemSmeltedEvent(ItemSmeltedEvent event) {
		if(ModTools.getWorld(event.player).isRemote) {
			SoundHelper.playSoundClient(new BlockPos(event.player), ModConfig.ItemSmeltedSound);
		}else if(ModConfig.ItemSmeltedSound.server) {
			SoundHelper.playSoundServerAllBut(new BlockPos(event.player), event.player, event.player.dimension, ModConfig.ItemSmeltedSound);
		}	
	}
	
	@SubscribeEvent
	public void onLivingJumpEvent(LivingJumpEvent event) {
		if(ModTools.getWorld(event.getEntityLiving()).isRemote) {
			SoundHelper.playSoundClient(new BlockPos(event.getEntity()), ModConfig.PlayerJumpSound);
		}else if(ModConfig.PlayerJumpSound.server) {
			SoundHelper.playSoundServerAllBut(new BlockPos(event.getEntity()), (event.getEntityLiving() instanceof EntityPlayer)?(EntityPlayer)event.getEntityLiving():null, event.getEntity().dimension, ModConfig.PlayerJumpSound);
		}	
	}
	
	@SubscribeEvent
	public void onPlayerSleepInBedEvent(PlayerSleepInBedEvent event) {
		if(ModTools.getWorld(event.getEntityPlayer()).isRemote) {
			SoundHelper.playSoundClient(event.getPos(), ModConfig.PlayerSleepInBedSound);
		}else if(ModConfig.PlayerSleepInBedSound.server) {
			SoundHelper.playSoundServerAllBut(event.getPos(), event.getEntityPlayer(), event.getEntity().dimension, ModConfig.PlayerSleepInBedSound);
		}	
	}
	
	@SubscribeEvent
	public void onPlayerWakeUpEvent(PlayerWakeUpEvent event) {
		if(ModTools.getWorld(event.getEntityPlayer()).isRemote) {
			SoundHelper.playSoundClient(new BlockPos(event.getEntity()), ModConfig.PlayerWakeUpSound);
		}else if(ModConfig.PlayerWakeUpSound.server) {
			SoundHelper.playSoundServerAllBut(new BlockPos(event.getEntity()), event.getEntityPlayer(), event.getEntity().dimension, ModConfig.PlayerWakeUpSound);
		}	
	}
	
	/*
	 * Server side only (still loaded on client for internal server) 
	 */
	
	private Map<UUID, StruckVal> struckMap = new HashMap<UUID, StruckVal>();
	
	@SubscribeEvent
	public void onXPPickup(PlayerPickupXpEvent event) {
		if(event.getEntityPlayer().experienceLevel % 5 != 4 &&event.getEntityPlayer().experience+(((float)event.getOrb().xpValue)/event.getEntityPlayer().xpBarCap()) > 1f) {
			if(ModConfig.PlayerLevelUpSound.server) {
				SoundHelper.playSoundServer(new BlockPos(event.getEntityPlayer()), null, event.getEntityPlayer().dimension, ModConfig.PlayerLevelUpSound);
			}else {
				SoundHelper.playSoundServer(new BlockPos(event.getEntityPlayer()), (EntityPlayerMP) event.getEntityPlayer(), event.getEntityPlayer().dimension, ModConfig.PlayerLevelUpSound);
			}
		}
	}
	
	@SubscribeEvent
	public void onBabyEntitySpawnEvent(BabyEntitySpawnEvent event) {
		SoundHelper.playSoundServer(new BlockPos(event.getChild()), null, event.getChild().dimension, ModConfig.BabyEntitySpawnSound);
	}
	
	@SubscribeEvent
	public void onCropGrowEvent(CropGrowEvent.Post event) {
		SoundHelper.playSoundServer(event.getPos(), null, event.getWorld().provider.getDimension(), ModConfig.CropGrowSound);
	}
	
	@SubscribeEvent
	public void onEntityMountEvent(EntityMountEvent event) {
		if(!event.isCanceled()) {
			if(event.getEntityMounting() instanceof EntityPlayer) {
				if(ModConfig.PlayerMountSound.server) {
					SoundHelper.playSoundServer(new BlockPos(event.getEntityBeingMounted()), null, event.getEntityBeingMounted().dimension, ModConfig.PlayerMountSound);
				}else {
					SoundHelper.playSoundServer(new BlockPos(event.getEntityBeingMounted()), (EntityPlayerMP) event.getEntityMounting(), event.getEntityBeingMounted().dimension, ModConfig.PlayerMountSound);
				}
			}else {
				SoundHelper.playSoundServer(new BlockPos(event.getEntityBeingMounted()), null, event.getEntityBeingMounted().dimension, ModConfig.PlayerMountSound);
			}
		}
	}
	
	@SubscribeEvent
	public void onEntityStruckByLightningEvent(EntityStruckByLightningEvent event) {
		if(this.struckMap.containsKey(event.getLightning().getUniqueID())&&this.struckMap.get(event.getLightning().getUniqueID()).vics.contains(event.getEntity().getUniqueID())) { // Make sure lightning bolt is not in the list
			StruckVal item = this.struckMap.get(event.getLightning().getUniqueID());
			item.time = ModTools.getWorld(event.getEntity()).getWorldTime();
			this.struckMap.put(event.getLightning().getUniqueID(), item);
		}else {
			if(this.struckMap.containsKey(event.getLightning().getUniqueID())) {
				StruckVal item = this.struckMap.get(event.getLightning().getUniqueID());
				item.vics.add(event.getEntity().getUniqueID());
				item.time = ModTools.getWorld(event.getEntity()).getWorldTime();
				this.struckMap.put(event.getLightning().getUniqueID(), item);
			}else {
				this.struckMap.put(event.getLightning().getUniqueID(), new StruckVal(ModTools.getWorld(event.getEntity()).getWorldTime(), event.getEntity().getUniqueID()));
			}
			List<UUID> removeList = new ArrayList<UUID>();
			for(UUID uuid : this.struckMap.keySet()) {// Clean list
				if(this.struckMap.get(uuid).time + 20 < ModTools.getWorld(event.getEntity()).getWorldTime()||this.struckMap.get(uuid).time > ModTools.getWorld(event.getEntity()).getWorldTime()) {
					removeList.add(uuid);
				}
			}
			for(UUID uuid : removeList) {
				this.struckMap.remove(uuid);
			}
			if(event.getEntity() instanceof EntityPlayer) {
			if(ModConfig.PlayerStruckByLightningSound.server) {
					SoundHelper.playSoundServer(new BlockPos(event.getEntity()), null, event.getEntity().dimension, ModConfig.PlayerStruckByLightningSound);
				}else {
					SoundHelper.playSoundServer(new BlockPos(event.getEntity()), (EntityPlayerMP) event.getEntity(), event.getEntity().dimension, ModConfig.PlayerStruckByLightningSound);
				}
			}else {
				SoundHelper.playSoundServer(new BlockPos(event.getEntity()), null, event.getEntity().dimension, ModConfig.EntityStruckByLightningSound);
			}
		}
	}
	
	@SubscribeEvent
	public void onEntityTravelToDimensionEvent(EntityTravelToDimensionEvent event) {
		if(!(event.getEntity() instanceof EntityPlayer)){
			SoundHelper.playSoundServer(new BlockPos(event.getEntity()), null, event.getEntity().dimension, ModConfig.EntityTravelToDimensionSound);
		}
	}
	
	@SubscribeEvent
	public void onItemExpireEvent(ItemExpireEvent event) {
		SoundHelper.playSoundServer(new BlockPos(event.getEntity()), null, event.getEntity().dimension, ModConfig.ItemExpireSound);
	}
	
	@SubscribeEvent
	public void onItemTossEvent(ItemTossEvent event) {
			SoundHelper.playSoundServer(new BlockPos(event.getEntity()), null, event.getEntity().dimension, ModConfig.ItemTossSound);
	}
	
	@SubscribeEvent
	public void onLivingExperienceDropEvent(LivingExperienceDropEvent event) {
		SoundHelper.playSoundServer(new BlockPos(event.getEntity()), null, event.getEntity().dimension, ModConfig.LivingExperienceDropSound);
	}
	
	@SubscribeEvent
	public void onPlayerChangedDimensionEvent(PlayerChangedDimensionEvent event) {
		if(ModConfig.PlayerChangedDimensionSound.server) {
			SoundHelper.playSoundServer(new BlockPos(event.player), null, event.player.dimension, ModConfig.PlayerChangedDimensionSound);
		}else {
			SoundHelper.playSoundServer(new BlockPos(event.player), (EntityPlayerMP) event.player, event.player.dimension, ModConfig.PlayerChangedDimensionSound);
		}
	}
	
	@SubscribeEvent
	public void onPlayerLoggedInEvent(PlayerLoggedInEvent event) {
		if(ModConfig.PlayerLoggedInSound.server) {
			SoundHelper.playSoundServer(new BlockPos(event.player), null, event.player.dimension, ModConfig.PlayerLoggedInSound);
		}else {
			SoundHelper.playSoundServer(new BlockPos(event.player), (EntityPlayerMP) event.player, event.player.dimension, ModConfig.PlayerLoggedInSound);
		}
	}
	
	@SubscribeEvent
	public void onPlayerLoggedOutEvent(PlayerLoggedOutEvent event) {
		if(ModConfig.PlayerLoggedOutSound.server) {
			SoundHelper.playSoundServerAllBut(new BlockPos(event.player), event.player, event.player.dimension, ModConfig.PlayerLoggedOutSound);
		}
	}
	
	@SubscribeEvent
	public void onPlayerRespawnEvent(PlayerRespawnEvent event) {
		if(ModConfig.PlayerRespawnSound.server) {
			SoundHelper.playSoundServer(new BlockPos(event.player), null, event.player.dimension, ModConfig.PlayerRespawnSound);
		}else {
			SoundHelper.playSoundServer(new BlockPos(event.player), (EntityPlayerMP) event.player, event.player.dimension, ModConfig.PlayerRespawnSound);
		}
	}
}
