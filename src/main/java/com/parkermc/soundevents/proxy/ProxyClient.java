package com.parkermc.soundevents.proxy;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import com.parkermc.soundevents.ExternalResourcePack;
import com.parkermc.soundevents.events.EventHandlerClient;

import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.IResourcePack;
import net.minecraft.client.resources.ResourcePackRepository;
import net.minecraft.client.resources.ResourcePackRepository.Entry;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

public class ProxyClient extends ProxyCommon {
	public static Configuration serverConfig = null;
	private EventHandlerClient clientEventHandler = new EventHandlerClient();
	
	public void preInit(FMLPreInitializationEvent event) {
		super.preInit(event);
		MinecraftForge.EVENT_BUS.register(clientEventHandler);
		try {
			Constructor<Entry> constructor = Entry.class.getDeclaredConstructor(ResourcePackRepository.class, IResourcePack.class);
	    	constructor.setAccessible(true);
	    	
	    	ResourcePackRepository rpreg = Minecraft.getMinecraft().getResourcePackRepository();
	    	
	    	List<Entry> rpentrys = new ArrayList<Entry>();
	    	rpentrys.addAll(rpreg.getRepositoryEntries());
	    	rpentrys.add(constructor.newInstance(rpreg, new ExternalResourcePack()));
	    	rpreg.setRepositories(rpentrys);
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
	}
	
	public void init(FMLInitializationEvent event) {
		super.init(event);
	}
	
	public void postInit(FMLPostInitializationEvent event) {
		super.postInit(event);
	}

}
