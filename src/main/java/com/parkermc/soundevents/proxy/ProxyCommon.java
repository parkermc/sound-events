package com.parkermc.soundevents.proxy;

import java.io.File;

import com.parkermc.soundevents.ModConfig;
import com.parkermc.soundevents.ModMain;
import com.parkermc.soundevents.events.EventHandler;
import com.parkermc.soundevents.events.EventHandlerAchievement;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

public class ProxyCommon {
	public static Configuration config;
	public static File packFolder;
	private EventHandler eventHandler = new EventHandler();
	private EventHandlerAchievement eventHandlerAchievement = new EventHandlerAchievement();
	
	public void preInit(FMLPreInitializationEvent event) {
		MinecraftForge.EVENT_BUS.register(eventHandler);
		MinecraftForge.EVENT_BUS.register(eventHandlerAchievement);
        File directory = event.getModConfigurationDirectory();
        packFolder = new File(directory.getPath(), ModMain.MODID);
        config = new Configuration(new File(directory.getPath(), ModMain.MODID+".cfg"));
        ModConfig.readConfig();
	}
	
	public void init(FMLInitializationEvent event) {
	}
	
	public void postInit(FMLPostInitializationEvent event) {
	}
}
