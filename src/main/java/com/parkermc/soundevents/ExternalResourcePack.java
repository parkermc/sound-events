package com.parkermc.soundevents;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import com.google.common.base.Charsets;
import com.google.common.collect.Sets;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.parkermc.soundevents.proxy.ProxyClient;

import net.minecraft.client.resources.IResourcePack;
import net.minecraft.client.resources.data.IMetadataSection;
import net.minecraft.client.resources.data.MetadataSerializer;
import net.minecraft.util.ResourceLocation;

public class ExternalResourcePack implements IResourcePack {
	private final File path = ProxyClient.packFolder;
	private List<String> fakeFiles = new ArrayList<String>(Arrays.asList("sounds.json"));

    @Override
    public String getPackName(){
        return "Sound Events pack";
    }
        
    @Override
    public Set<String> getResourceDomains(){
    	// Do pack setup stuffs
    	if(!this.path.exists()) {
    		this.path.mkdirs();
    		List<String> lines = Arrays.asList("Place sound files here in .ogg format.",
    				"All files will be placed in the soundevents: namespace",
    				"If you set the file name to a sound name e.g.\"chat_recived_sound.ogg\" the sound will automatically be set if it is not already set");
    		try {
				Files.write(new File(this.path, "README").toPath(), lines, Charset.forName("UTF-8"));
			} catch (IOException e) {
				e.printStackTrace();
			}
    	}
        return Sets.<String>newHashSet("soundevents");
    }
    
    @Override
    public <T extends IMetadataSection> T getPackMetadata(MetadataSerializer metadataSerializer, String metadataSectionName) throws IOException{
    	JsonObject jsonobject = null;
        try{
            jsonobject = (new JsonParser()).parse("{\n" +
                    " \"pack\": {\n"+
                    "   \"description\": \"dummy FML pack for Sound Events\",\n"+
                    "   \"pack_format\": 1\n"+
                    "}\n" +
                    "}").getAsJsonObject();
        }catch (RuntimeException runtimeexception){
            throw new JsonParseException(runtimeexception);
        }

        return metadataSerializer.parseMetadataSection(metadataSectionName, jsonobject);
    }
    
    @Override
    public boolean resourceExists(ResourceLocation location){
        return this.fakeFiles.contains(location.getResourcePath())||(new File(this.path, location.getResourcePath().replaceFirst("sounds/custom", ""))).isFile();
    }
    
    @Override
    public InputStream getInputStream(ResourceLocation location) throws IOException{
    	if(location.getResourcePath().equals("sounds.json")) {
    		return new ByteArrayInputStream(addToJson(this.path, new JsonObject(), "").toString().getBytes(Charsets.UTF_8));
    	}
        try{
        	return new BufferedInputStream(new FileInputStream(new File(this.path, location.getResourcePath().replaceFirst("sounds/custom", ""))));
        }catch (IOException ioe){
            throw ioe;
        }
    }
    
    private static JsonObject addToJson(File path, JsonObject json, String dir) {
    	for(File file : path.listFiles()) {
			if(file.isFile()) {
				if(file.getName().endsWith(".ogg")) {
					String name = file.getName().substring(0, file.getName().length()-4);
					JsonArray arr = new JsonArray();
					arr.add(new JsonPrimitive("soundevents:custom/"+dir+name));
					JsonObject obj = new JsonObject();
					obj.add("sounds", arr);
					json.add(dir+name, obj);
				}
			}else if(file.isDirectory()) {
				json = addToJson(file, json, dir+file.getName()+"/");
			}
		}
    	return json;
    }
    
    @Override
    public BufferedImage getPackImage() throws IOException{
        return null;
    }
}