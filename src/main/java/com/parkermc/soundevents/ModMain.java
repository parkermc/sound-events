package com.parkermc.soundevents;

import com.parkermc.soundevents.network.MessageConfig;
import com.parkermc.soundevents.network.MessageSound;
import com.parkermc.soundevents.proxy.ProxyCommon;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

@Mod(modid = ModMain.MODID, name = ModMain.MODNAME, version = ModMain.VERSION, guiFactory = "com.parkermc.soundevents.gui.GuiModConfigFactory")
public class ModMain{
    public static final String MODID = "soundevents";
    public static final String MODNAME = "Sound Events";
    public static final String VERSION = "1.0.3";
    
    @SidedProxy(clientSide = "com.parkermc.soundevents.proxy.ProxyClient", serverSide = "com.parkermc.soundevents.proxy.ProxyServer", modId = MODID)
    public static ProxyCommon proxy;
    
    public static SimpleNetworkWrapper network; 
    
    @EventHandler
    public void preInit(FMLPreInitializationEvent event){
    	proxy.preInit(event);
    	network = NetworkRegistry.INSTANCE.newSimpleChannel(MODID);
    	network.registerMessage(MessageConfig.Handler.class, MessageConfig.class, 0, Side.CLIENT);
    	network.registerMessage(MessageSound.Handler.class, MessageSound.class, 1, Side.CLIENT);
    }
    
    @EventHandler
    public void init(FMLInitializationEvent event) {
    	proxy.init(event);   
    }
    
    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
    	proxy.postInit(event);
    }
}

