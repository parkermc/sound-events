package com.parkermc.soundevents;

import java.io.File;

import com.parkermc.soundevents.proxy.ProxyClient;
import com.parkermc.soundevents.proxy.ProxyCommon;
import com.parkermc.soundevents.sound.SoundConfig;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class ModConfig {
	private static final String CATEGORY_GENERAL = "general";
	public static boolean allowOverride = true;
	
	public static SoundConfig ClientChatReceivedSound;
	public static SoundConfig ScreenshotSound;
	
	public static SoundConfig AchievementSound;
	public static SoundConfig BabyEntitySpawnSound;
	public static SoundConfig CropGrowSound;
	public static SoundConfig EntityMountSound;
	public static SoundConfig PlayerMountSound;
	public static SoundConfig EntityStruckByLightningSound;
	public static SoundConfig PlayerStruckByLightningSound;
	public static SoundConfig EntityTravelToDimensionSound;
	public static SoundConfig ItemExpireSound;
	public static SoundConfig ItemTossSound;
	public static SoundConfig LivingExperienceDropSound;
	public static SoundConfig PlayerChangedDimensionSound;
	public static SoundConfig PlayerLoggedInSound;
	public static SoundConfig PlayerLoggedOutSound;
	public static SoundConfig PlayerRespawnSound;
	
	public static SoundConfig ItemCraftedSound;
	public static SoundConfig ItemSmeltedSound;
	public static SoundConfig PlayerJumpSound;
	public static SoundConfig PlayerSleepInBedSound;
	public static SoundConfig PlayerWakeUpSound;
	public static SoundConfig PlayerLevelUpSound;

    public static void readConfig() {
        Configuration cfg = ProxyCommon.config;
        try {
            cfg.load();
            initGeneralConfig(cfg);
        } catch (Exception e1) {
            ModTools.logError("Problem loading config file!", e1);
        } finally {
            if (cfg.hasChanged()) {
                cfg.save();
                cfg.load();
            }
        }
        if(ProxyClient.serverConfig != null){
        	initGeneralConfig(ProxyClient.serverConfig);
        }
    }
    
    private static void initGeneralConfig(Configuration cfg) {
        cfg.addCustomCategoryComment(CATEGORY_GENERAL, "General configuration\n\n" +
        		"For each sound either write the sound like \"minecraft:entity.ghast.shoot\"\n\n" +
        		"If the server setting is set to true the sound is played to all players on the server from the current position (disabled or always on for some sounds)\n" +
        		"If positioned is true the sound will happen at the possition of the event that triggered it (disabled for some sounds)\n" +
        		"\nNote: You can add sounds to the soundevents forlder instead of adding a resoruce pack read the README for more info\n\n" +
        		"Note the following are possible options for the sound category \n" +
        		"master\n" + 
        		"music\n" + 
        		"record\n" + 
        		"weather\n" + 
        		"block\n" + 
        		"hostile\n" + 
        		"neutral\n" + 
        		"player\n" + 
        		"ambient\n" + 
        		"voice");
        
        allowOverride = cfg.getBoolean("allow_override", CATEGORY_GENERAL, allowOverride, "Allows the server to override the client config (Note that to improve performance if the sound is blank on the server it will not play the sound if this is an issue for someone it can be changed just ask!)");
        
        ClientChatReceivedSound = getSound(cfg, "chat_recived_sound", CATEGORY_GENERAL, false, false, "Plays when a player receives a chat message (Client only)");
    	ScreenshotSound = getSound(cfg, "screenshot_sound", CATEGORY_GENERAL, false, false, "Plays when a player takes a screenshot (Client only)");
    	
    	if(!Loader.MC_VERSION.contains("1.12")) {
    		AchievementSound = getSound(cfg, "achievement_sound", CATEGORY_GENERAL, true, true, "Plays when a player gets and achievement (Server Only)");
    	}
    	BabyEntitySpawnSound = getSound(cfg, "baby_entity_spawn_sound", CATEGORY_GENERAL, false, true, "Plays when a baby entity is spawned (Server Only)");
    	CropGrowSound = getSound(cfg, "crop_grow_sound", CATEGORY_GENERAL, false, true, "Plays when a crop grows (Server Only)");
    	EntityMountSound = getSound(cfg, "entity_mount_sound", CATEGORY_GENERAL, false, true, "Plays when an entity (not player) mounts another entity (Server Only) - Not tested (no idea how lol) but should work");
    	EntityStruckByLightningSound = getSound(cfg, "entity_struck_by_lightning_sound", CATEGORY_GENERAL, false, true, "Plays when an entity (not player) is struck by lightning (Server Only)");
    	EntityTravelToDimensionSound = getSound(cfg, "entity_dimension_change_sound", CATEGORY_GENERAL, false, true, "Plays when an entity (not player) changes dimensions (Server Only)");
    	ItemExpireSound = getSound(cfg, "item_expire_sound", CATEGORY_GENERAL, false, true, "Plays when an item despawns due to time on ground (Server Only)");
    	ItemTossSound = getSound(cfg, "item_drop_sound", CATEGORY_GENERAL, true, false, "Plays when a player drops/tosses an item (Server Only)");
    	LivingExperienceDropSound = getSound(cfg, "entity_xp_drop_sound", CATEGORY_GENERAL, false, true, "Plays when xp is dropped from an entity (Server Only)");
    	PlayerChangedDimensionSound = getSound(cfg, "player_dimension_change_sound", CATEGORY_GENERAL, true, true, "Plays when a player changes dimensions (Server Only)");
    	PlayerLoggedInSound = getSound(cfg, "player_logged_in_sound", CATEGORY_GENERAL, true, true, "Plays when a player logs in (Server Only)");
    	PlayerLoggedOutSound = getSound(cfg, "player_logged_out_sound", CATEGORY_GENERAL, true, true, "Plays when a player logs out (Server Only)");
    	PlayerMountSound = getSound(cfg, "player_mount_sound", CATEGORY_GENERAL, true, true, "Plays when a player mounts an entity (Server Only)");
    	PlayerRespawnSound = getSound(cfg, "player_respawn_sound", CATEGORY_GENERAL, true, true, "Plays when a player respawns (Server Only)");
    	PlayerStruckByLightningSound = getSound(cfg, "player_struck_by_lightning_sound", CATEGORY_GENERAL, true, true, "Plays when a player is struck by lightning (Server Only)");
        
        ItemCraftedSound = getSound(cfg, "crafted_sound", CATEGORY_GENERAL, true, false, "Plays when a player crafts an item");
        ItemSmeltedSound = getSound(cfg, "item_smelted_sound", CATEGORY_GENERAL, true, false, "Plays when a smelted item is taken out of a furnace");
        PlayerJumpSound = getSound(cfg, "player_jump_sound", CATEGORY_GENERAL, true, false, "Plays when a player jumps");
        PlayerSleepInBedSound = getSound(cfg, "player_sleep_sound", CATEGORY_GENERAL, true, false, "Plays when a player sleep in a bed");
        PlayerWakeUpSound = getSound(cfg, "player_wakeup_sound", CATEGORY_GENERAL, true, false, "Plays when a player wakes up");
        
        PlayerLevelUpSound = getSound(cfg, "player_levelup_sound", CATEGORY_GENERAL, true, false, "Plays when a player levels up(doesn't happen for a multiple of 5 as mc already does this)");
    }
    
    private static SoundConfig getSound(Configuration cfg, String cfgName, String cfgCategory, boolean allowClientOption, boolean defaultServer, String comment) {
    	ResourceLocation name = new ResourceLocation(cfg.getString(cfgName, cfgCategory, "", comment));
    	SoundCategory category = SoundCategory.getByName(cfg.getString(cfgName+"_category", cfgCategory, SoundCategory.MUSIC.getName(), ""));
    	if(category == null) {
    		category = SoundCategory.MUSIC;
    	}
    	float volume = cfg.getFloat(cfgName+"_volume", cfgCategory, 1, 0, 255, "");
    	float pitch = cfg.getFloat(cfgName+"_pitch", cfgCategory, 1, 0, 2, "");
    	boolean server = (!allowClientOption)? defaultServer : cfg.getBoolean(cfgName+"_sever", cfgCategory, defaultServer, "");
    	boolean positioned = ((!allowClientOption)&&(!defaultServer))?false:cfg.getBoolean(cfgName+"_positioned", cfgCategory, true, "");
    	if(name.getResourcePath().isEmpty()&&new File(ProxyCommon.packFolder, cfgName+".ogg").exists()) {
    		name = new ResourceLocation(ModMain.MODID, cfgName);
    	}
    	return new SoundConfig(name, category, volume, pitch, server, positioned);
    }
    
    @Mod.EventBusSubscriber
	static class ConfigurationHandler {
		@SubscribeEvent
		public static void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent event) {
			if (event.getModID().equals(ModMain.MODID) && ProxyCommon.config.hasChanged()) {
				ProxyCommon.config.save();
				readConfig();
			}
		}
	}
}
