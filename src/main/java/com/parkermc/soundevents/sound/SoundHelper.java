package com.parkermc.soundevents.sound;

import javax.annotation.Nullable;

import com.parkermc.soundevents.ModMain;
import com.parkermc.soundevents.ModTools;
import com.parkermc.soundevents.network.MessageSound;

import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.ISound;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class SoundHelper {
	@SideOnly(Side.CLIENT)
	public static void playSoundClient(BlockPos pos, SoundConfig soundCfg) {
		if(!soundCfg.sound.getResourcePath().isEmpty()) {
			PositionedSoundRecord sound;
			if(!soundCfg.positioned) {
				sound = new PositionedSoundRecord(soundCfg.sound,  soundCfg.category, soundCfg.volume, soundCfg.pitch, false, 0, ISound.AttenuationType.NONE, 0, 0, 0);
			}else {	
		        sound = new PositionedSoundRecord(soundCfg.sound,  soundCfg.category, soundCfg.volume, soundCfg.pitch, false, 0, ISound.AttenuationType.LINEAR, (float)pos.getX() + 0.5F, (float)pos.getY() + 0.5F, (float)pos.getZ() + 0.5F);			
			}
			sound.createAccessor(Minecraft.getMinecraft().getSoundHandler());
			if(sound.getSound() != null) {
				Minecraft.getMinecraft().getSoundHandler().playSound(sound);
			}
		}
	}
	
	public static void playSoundServer(BlockPos pos, @Nullable EntityPlayerMP player, int dim, SoundConfig soundCfg) {
		if(!soundCfg.sound.getResourcePath().isEmpty()) {
			if(player == null) {
				if(soundCfg.positioned) {
					ModMain.network.sendToDimension(new MessageSound(soundCfg, pos), dim);
				}else {
					ModMain.network.sendToAll(new MessageSound(soundCfg, pos));
				}
			}else {
				ModMain.network.sendTo(new MessageSound(soundCfg, pos), player);
			}
		}
	}
	
	public static void playSoundServerAllBut(BlockPos pos, EntityPlayer player, int dim, SoundConfig soundCfg) {
		for(EntityPlayer msgPlayer : (soundCfg.positioned)?ModTools.getPlayersInDim(dim):ModTools.getAllPlayers()) {
			if(!msgPlayer.getUniqueID().equals(player.getUniqueID())) {
				ModMain.network.sendTo(new MessageSound(soundCfg, pos), (EntityPlayerMP)msgPlayer);
			}
		}
	}
}
