package com.parkermc.soundevents.sound;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;

public class SoundConfig {
	public final ResourceLocation sound;
	public final SoundCategory category;
	public final float volume;
	public final float pitch;
	public final boolean server;
	public final boolean positioned;
	
	public SoundConfig(ResourceLocation sound, SoundCategory category, float volume, float pitch, boolean server, boolean positioned) {
		this.sound = sound; 
		this.volume = volume;
		this.pitch = pitch;
		this.category = category;
		this.server = server;
		this.positioned = positioned;
	}
}
