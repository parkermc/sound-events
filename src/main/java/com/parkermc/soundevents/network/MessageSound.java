package com.parkermc.soundevents.network;

import com.parkermc.soundevents.sound.SoundConfig;
import com.parkermc.soundevents.sound.SoundHelper;

import io.netty.buffer.ByteBuf;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class MessageSound implements IMessage {

	SoundConfig sound;
	BlockPos blockpos;
	
	public MessageSound(){
	}
	
	public MessageSound(SoundConfig sound, BlockPos blockpos){
		this.sound = sound;
		this.blockpos = blockpos;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		ResourceLocation sound = new ResourceLocation(ByteBufUtils.readUTF8String(buf));
		SoundCategory category = SoundCategory.getByName(ByteBufUtils.readUTF8String(buf));
		float volume = buf.readFloat();
		float pitch = buf.readFloat();
		boolean server = buf.readBoolean();
		boolean positioned = buf.readBoolean();
		this.sound = new SoundConfig(sound, category, volume, pitch, server, positioned);
		
		int x = buf.readInt();
		int y = buf.readInt();
		int z = buf.readInt();
		this.blockpos = new BlockPos(x, y, z);
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeUTF8String(buf, this.sound.sound.toString());
		ByteBufUtils.writeUTF8String(buf, this.sound.category.getName());
		buf.writeFloat(this.sound.volume);
		buf.writeFloat(this.sound.pitch);
		buf.writeBoolean(this.sound.server);
		buf.writeBoolean(this.sound.positioned);
		buf.writeInt(blockpos.getX());
		buf.writeInt(blockpos.getY());
		buf.writeInt(blockpos.getZ());
	}
	
	public static class Handler implements IMessageHandler<MessageSound, IMessage>{

		@SideOnly(Side.CLIENT)
		@Override
		public IMessage onMessage(MessageSound message, MessageContext ctx) {
			SoundHelper.playSoundClient(message.blockpos, message.sound);
			return null;
		}
		
	}
}
