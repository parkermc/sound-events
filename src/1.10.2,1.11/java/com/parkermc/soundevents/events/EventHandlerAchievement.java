package com.parkermc.soundevents.events;

import com.parkermc.soundevents.ModConfig;
import com.parkermc.soundevents.sound.SoundHelper;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.event.entity.player.AchievementEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class EventHandlerAchievement {
	@SubscribeEvent
	public void onAchievementEvent(AchievementEvent event) {
		if(!event.getEntityPlayer().hasAchievement(event.getAchievement())) {
			if(ModConfig.AchievementSound.server) {
				SoundHelper.playSoundServer(new BlockPos(event.getEntityPlayer()), null, event.getEntityPlayer().dimension, ModConfig.AchievementSound);
			}else {
				SoundHelper.playSoundServer(new BlockPos(event.getEntityPlayer()), (EntityPlayerMP) event.getEntityPlayer(), event.getEntityPlayer().dimension, ModConfig.AchievementSound);
			}
		}
	}
}
