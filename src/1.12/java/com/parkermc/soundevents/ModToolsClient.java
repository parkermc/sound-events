package com.parkermc.soundevents;

import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ChatType;
import net.minecraftforge.client.event.ClientChatReceivedEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class ModToolsClient {
	
	public static BlockPos getClientPos() {
		return new BlockPos(Minecraft.getMinecraft().player);
	}
	
	public static WorldClient getWorld() {
		return Minecraft.getMinecraft().world;
	}
	
	public static boolean isChatMsg(ClientChatReceivedEvent event) {
		return event.getType() == ChatType.CHAT;
	}
}