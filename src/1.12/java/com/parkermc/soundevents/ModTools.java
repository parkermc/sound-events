package com.parkermc.soundevents;

import java.util.List;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.FMLLog;

public class ModTools {
	
	public static List<EntityPlayerMP> getAllPlayers() {
		return FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayers();
	}
	
	public static List<EntityPlayer> getPlayersInDim(int dim){
		return FMLCommonHandler.instance().getMinecraftServerInstance().getWorld(dim).playerEntities;
	}
	
	public static World getWorld(Entity entity) {
		return entity.world;
	}
	
	public static void logError(String format, Object... data) {
		FMLLog.log.error(format, data);
	}
}